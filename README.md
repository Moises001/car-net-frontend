﻿# CAR-NET Frontend
Aplicación web que consume los servicios del API "CAR-NET Backend"

## Primeros pasos

* Clonar el repositorio que se encuentra en: [CAR-NET Frontend](https://bitbucket.org/Moises001/car-net-frontend/src/master/)


### Prerequisitos
* [npm](https://www.npmjs.com/)

### Instalación

* Ubicarse en el directorio donde se encuentra el repositorio y ejecutar los siguientes comandos:
```
# Instalar dependencias
npm install

# Correr el servicio en localhost:8080
npm run dev
```


* Una vez arriba la maquina virtual ir a la direccion: localhost:8080 

![Alt text](src/assets/vuecarnet.png)

Recuerde que debe crear alumnos y eventos para empezar a consumirlos desde el API.


## Despliegue

Para realizar el despliegue, es necesario cambiar las rutas de las siguientes vistas: Button.vue, ButtonVerEvento.vue, CameraView.vue, EventsList.vue, Stamps.vue, AlumnoView.vue y ListaEventosView.vue. 
```
   mounted () {
      axios.get('tu_ruta_de_despliegue/listarEventos').then(response => (this.items = response.data))
    }
```


## Construido con
* [Vue.js](https://vuejs.org/) - Framework
* [Vuetify](https://vuetifyjs.com/en/) - Usado como componente de diseño
* [axios](https://github.com/axios/axios) - Usado como cliente HTTP
* [vue-qrcode-reader](https://www.npmjs.com/package/vue-qrcode-reader?fbclid=IwAR2lr6Thpohd49YtwQYbCz8hf4hYmQQGZFmSaIgJy1uuQyLDL5g0LErvsoA) - Usado para la lectura de códigos QR

## Versionamiento

Usamos [Git](https://git-scm.com/) para versionamiento. Para las versiones disponibles, ir a [CAR-NET Frontend](https://bitbucket.org/Moises001/car-net-frontend/src/master/). 

## Autores

* **Moises Sandoval Contreras** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Saúl Daniel Silva García** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Evelyn Hull Valdez** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Jose Adrian Palomares Luevano** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Octavio Eduardo Hernandez Sixto** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)
* **Cesar Gonzalez Barreras** - [UABC](http://ingenieria.mxl.uabc.mx/pe_ico/)


## Agradecimientos

* Luis Enrique Vizcarra Corral - Sponsor del proyecto
* Jose Martín Olguín Espinoza - Supervisor del proyecto
