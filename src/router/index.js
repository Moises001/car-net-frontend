import Vue from 'vue'
import Router from 'vue-router'

// eslint-disable-next-line
//import Header from '@/components/Header/Header'
import HelloWorld from '@/components/HelloWorld'
import LogInView from '@/components/LogInView'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Login',
      name: 'LogIn',
      component: LogInView
    }
  ]
})
